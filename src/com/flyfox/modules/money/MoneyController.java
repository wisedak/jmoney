package com.flyfox.modules.money;

import java.util.List;

import com.flyfox.jfinal.base.BaseController;
import com.flyfox.jfinal.component.db.SQLUtils;
import com.flyfox.modules.project.TbProject;
import com.flyfox.util.DateUtils;
import com.flyfox.util.NumberUtils;
import com.flyfox.util.StrUtils;
import com.jfinal.plugin.activerecord.Page;

/**
 * 金钱管理
 * 
 * @author flyfox 2014-2-11
 */
public class MoneyController extends BaseController {

	private static final String path = "/pages/money/";

	public void list() {
		int project_id = 0;
		TbMoney model = getModel(TbMoney.class, "attr");

		SQLUtils sql = new SQLUtils(" from tb_money t,tb_project p where t.project_id = p.id ");
		if (model.getAttrValues().length != 0) {
			sql.setAlias("t");
			sql.whereLike("name", model.getStr("name"));
			sql.whereEquals("type", model.getStr("type"));
			project_id = NumberUtils.parseInt(model.get("project_id"));
			sql.whereEquals("project_id", project_id);

			String pay_time_start = getPara("pay_time_start");
			String pay_time_end = getPara("pay_time_end");
			model.put("pay_time_start", pay_time_start);
			model.put("pay_time_end", pay_time_end);
			if (StrUtils.isNotEmpty(pay_time_start))
				sql.append(" and pay_time >= '" + pay_time_start + "'");
			if (StrUtils.isNotEmpty(pay_time_end))
				sql.append(" and pay_time <= '" + pay_time_end + "'");
		}

		sql.append(" order by t.project_id,t.type,t.id desc");

		Page<TbMoney> page = TbMoney.dao.paginate(getPaginator(), "select t.*,p.name as projectName ", //
				sql.toString().toString());

		// 查询下拉框
		setAttr("optionList", selectProject(project_id));
		setAttr("page", page);
		setAttr("attr", model);
		render(path + "list.html");
	}

	public void add() {
		setAttr("projectName", selectProject(0));
		render(path + "add.html");
	}

	public void view() {
		TbMoney model = TbMoney.dao.findById(getParaToInt());
		setAttr("model", model);
		TbProject project = TbProject.dao.findById(model.getInt("project_id"));
		setAttr("projectName", project.getStr("name"));
		render(path + "view.html");
	}

	public void delete() {
		TbMoney.dao.deleteById(getParaToInt());
		list();
	}

	public void edit() {
		TbMoney model = TbMoney.dao.findById(getParaToInt());
		setAttr("model", model);
		setAttr("projectName", selectProject(model.getInt("project_id")));
		render(path + "edit.html");
	}

	public void save() {
		Integer pid = getParaToInt();
		TbMoney model = getModel(TbMoney.class);
		if (pid != null && pid > 0) { // 更新
			model.update();
		} else { // 新增
			model.remove("id");
			model.put("create_id", getSessionUser().getUserID());
			model.put("create_time", DateUtils.getNow());
			model.save();
		}
		renderMessage("保存成功");
	}
	
	/**
	 * 项目复选框
	 * 
	 * 2015年1月28日 下午5:28:40 flyfox 330627517@qq.com
	 * 
	 * @return
	 */
	private String selectProject(Integer selected) {
		List<TbProject> list = TbProject.dao.find(" select id,name from tb_project order by id ");
		StringBuffer sb = new StringBuffer("");
		if (list != null && list.size() > 0) {
			for (TbProject project : list) {
				sb.append("<option value=\"");
				sb.append(project.getInt("id"));
				sb.append("\" ");
				sb.append(project.getInt("id") == selected ? "selected" : "");
				sb.append(">");
				sb.append(project.getStr("name"));
				sb.append("</option>");
			}
		}
		return sb.toString();
	}
}
